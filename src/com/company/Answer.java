package com.company;

public class Answer {
	private boolean isCorrect = false;
	private String answerOption;
	private boolean isPicked;

	public Answer(String answerOption) {
		this.answerOption = answerOption;
	}

	public boolean isCorrect() {
		return isCorrect;
	}

	public void setCorrect(boolean correct) {
		isCorrect = correct;
	}

	public String getAnswerOption() {
		return answerOption;
	}

	public boolean isPicked() {
		return isPicked;
	}

	public void setPicked(boolean picked) {
		isPicked = picked;
	}

}
