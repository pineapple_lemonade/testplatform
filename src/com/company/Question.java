package com.company;

import java.util.ArrayList;

public class Question {
	int scoreForACorrectAnswer;
	ArrayList<Answer> answerOptionsList = new ArrayList<>();
	String questionName;
	boolean isAnsweredRight;
	Answer rightAnswer;
	boolean isMulti;
	int amountOfCorrectAnswers;

	public Question(int scoreForACorrectAnswer, String questionName) {
		this.scoreForACorrectAnswer = scoreForACorrectAnswer;
		this.questionName = questionName;
	}
}
