package com.company;

import java.util.Scanner;
import java.text.SimpleDateFormat;
import java.util.*;

public class InteractionWithUser {
	Scanner scanner = new Scanner(System.in);
	public static final String ANSI_RESET = "\u001B[0m";
	public static final String ANSI_RED = "\u001B[31m";
	public static final String ANSI_GREEN = "\u001B[32m";
	void userCall() {
		for (; ; ) {
			System.out.println("What command?(1 for start)");
			String command = scanner.next();
			switch (command) {
				case "1":
					printInfo();
				case "2":
					createQuestionsAndWriteData();
				case "3":
					testingProcess();
					break;
				case "4":
					System.out.println("bye");
					return;
				default:
					System.out.println("wrong");
			}
		}
	}
	void createQuestionsAndWriteData() {
		for (int j = 0; ; j++) {
			System.out.println("Do you want to write question?Y(or something else)/N");
			String command1 = scanner.next();
			if (command1.equalsIgnoreCase("n"))
				break;
			System.out.println("Enter your question name:");
			String questionName = scanner.next();
			DatabaseForQuestions.questionsList.add(new Question(0,questionName));
			System.out.println("Now u can add answer options, write how much answers u want to add");
			int amountOfAnswers = scanner.nextInt();
			System.out.println("is there will be multi right answers?Y/N(or something else)");
			String ifSeveralAnswers = scanner.next();
			System.out.println("Now u can write answers themselves");
			for (int i = 0; i < amountOfAnswers; i++) {
				DatabaseForQuestions.questionsList.get(j).answerOptionsList.add(new Answer(scanner.next()));
			}
			for (int i = 0; i < amountOfAnswers; i++) {
				System.out.println(i + "." + DatabaseForQuestions.questionsList.get(j).answerOptionsList.get(i).getAnswerOption());
			}
			if (ifSeveralAnswers.equalsIgnoreCase("y")) {
				System.out.println("Write how much answers you want to tag as correct");
				DatabaseForQuestions.questionsList.get(j).isMulti = true;
				int amountOfCorrectAnswers = scanner.nextInt();
				DatabaseForQuestions.questionsList.get(j).amountOfCorrectAnswers = amountOfCorrectAnswers;
				System.out.println("and score for a full correct answer, minimum is:"+amountOfCorrectAnswers);
				int score = scanner.nextInt();
				while (score<amountOfCorrectAnswers){
					System.out.println("wrong");
					score = scanner.nextInt();
				}
				DatabaseForQuestions.questionsList.get(j).scoreForACorrectAnswer = score;
				System.out.println("Enter the numbers of correct answers");
				for (int i = 0; i < amountOfCorrectAnswers; i++) {
					int multiNumberOfCorrectAnswer = scanner.nextInt();
					DatabaseForQuestions.questionsList.get(j).answerOptionsList.get(multiNumberOfCorrectAnswer).setCorrect(true);
				}
			} else {
				System.out.println("Please select the correct one by typing his number:");
				int numberOfCorrectAnswer = scanner.nextInt();
				System.out.println("and a score for a correct answer");
				DatabaseForQuestions.questionsList.get(j).scoreForACorrectAnswer = scanner.nextInt();
				DatabaseForQuestions.questionsList.get(j).answerOptionsList.get(numberOfCorrectAnswer).setCorrect(true);
				DatabaseForQuestions.questionsList.get(j).rightAnswer = DatabaseForQuestions.questionsList.get(j).answerOptionsList.get(numberOfCorrectAnswer);
			}
			System.out.println("The question is written");
		}
	}

	void testingProcess() {
		Date start = new Date();
		SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
		timeFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
		System.out.println("You can answer some questions, how much?");
		int amountOfQuestions = scanner.nextInt();
		while (amountOfQuestions > DatabaseForQuestions.questionsList.size()) {
			System.out.println("I dont have such amount of questions, try less");
			amountOfQuestions = scanner.nextInt();
		}
		Collections.shuffle(DatabaseForQuestions.questionsList);
		for (int i = 0; i < amountOfQuestions; i++) {
			DatabaseForQuestions.scores = 0;
			int counter = 0;
			System.out.println(DatabaseForQuestions.questionsList.get(i).questionName);
			for (int j = 0; j < DatabaseForQuestions.questionsList.get(i).answerOptionsList.size(); j++) {
				System.out.println(j + "." + DatabaseForQuestions.questionsList.get(i).answerOptionsList.get(j).getAnswerOption());//вывод вариантов ответа
			}
			System.out.println("type your answer/answers by choosing the number/numbers before dot");
			if (DatabaseForQuestions.questionsList.get(i).isMulti) {//сценарий мультивыбора
				System.out.println("For this question the amount of correct answers is:" + DatabaseForQuestions.questionsList.get(i).amountOfCorrectAnswers);//сколько надо ввести ответов
				System.out.println("so, write your answers by typing number of it");
				for (int j = 0; j < DatabaseForQuestions.questionsList.get(i).amountOfCorrectAnswers; j++) {
					int answer = scanner.nextInt();
					DatabaseForQuestions.questionsList.get(i).answerOptionsList.get(answer).setPicked(true);//помечаем пикнутые ответы
					if (DatabaseForQuestions.questionsList.get(i).answerOptionsList.get(answer).isCorrect()) {
						counter++;//считаем количество верных
					}
				}
				if (counter == DatabaseForQuestions.questionsList.get(i).amountOfCorrectAnswers) {
					DatabaseForQuestions.scores = counter;
				}
				if(counter == 0){
					DatabaseForQuestions.scores = 0;
				}
				else{
					DatabaseForQuestions.scores = DatabaseForQuestions.questionsList.get(i).scoreForACorrectAnswer - DatabaseForQuestions.questionsList.get(i).amountOfCorrectAnswers + counter;
				}
				if (DatabaseForQuestions.questionsList.get(i).isMulti && counter != DatabaseForQuestions.questionsList.get(i).amountOfCorrectAnswers) {
					System.out.println("you have typed " + counter + " right answers and there is other correct answers:");
					for (int j = 0; j < DatabaseForQuestions.questionsList.get(i).answerOptionsList.size(); j++) {
						if (!DatabaseForQuestions.questionsList.get(i).answerOptionsList.get(j).isPicked() && DatabaseForQuestions.questionsList.get(i).answerOptionsList.get(j).isCorrect()) {
							System.out.println(ANSI_GREEN + DatabaseForQuestions.questionsList.get(i).answerOptionsList.get(j).getAnswerOption() + ANSI_RESET);//остальные правильные ответы
						}
					}
				}
			} else {
				int answer = scanner.nextInt();//стандартный сценарий одного варианта ответа
				if (DatabaseForQuestions.questionsList.get(i).answerOptionsList.get(answer).isCorrect()) {
					DatabaseForQuestions.scores += DatabaseForQuestions.questionsList.get(i).scoreForACorrectAnswer;
					DatabaseForQuestions.questionsList.get(i).isAnsweredRight = true;
				}
				DatabaseForQuestions.questionsList.get(i).answerOptionsList.get(answer).setPicked(true);
				if (!DatabaseForQuestions.questionsList.get(i).isAnsweredRight) {
					System.out.println("You give wrong answer for this question:" + DatabaseForQuestions.questionsList.get(i).questionName + " the correct one is:");
					System.out.println(ANSI_GREEN + DatabaseForQuestions.questionsList.get(i).rightAnswer.getAnswerOption() + ANSI_RESET);
				}
			}
		}
		Date finish = new Date();
		String timeString = timeFormat.format(new Date(finish.getTime() - start.getTime()));
		System.out.println("You have passed the test for: " + timeString + " with " + DatabaseForQuestions.scores + " scores");
	}
	void printInfo(){
		System.out.println("Here is the list of commands that u can use:");
		System.out.println("2 is for making questions and then opportunity to answer them");
		System.out.println("3 is for answering questions, but without making them, you can't answer because they didnt exist");
		System.out.println("4 is for exit");
	}
}
